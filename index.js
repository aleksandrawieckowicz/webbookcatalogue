var table;
var rIndex;


function defineVariables() {
    table = document.getElementById("tableOfProducts");
}

function checkEmptyInput() {
    var isEmpty = false;
    var editTabCells = ["tittle", "autor", "type", "price", "pages", "cover"];
    for (var i = 0; i < 6; i++) {
        if (document.getElementById(editTabCells[i]).value === "") {
            alert("Pole nie może być puste");
            isEmpty = true;
        }
    }
    return isEmpty;
}

function addHtmlTableRow() {
    if (!checkEmptyInput()) {
        var types = document.getElementById("types");
        var option = document.createElement("option");

        var newRow = table.insertRow(table.lenght),
            cell1 = newRow.insertCell(0),
            cell2 = newRow.insertCell(1),
            cell3 = newRow.insertCell(2),
            cell4 = newRow.insertCell(3),
            cell5 = newRow.insertCell(4),
            cell6 = newRow.insertCell(5),

            tittle = document.getElementById("tittle").value;
        autor = document.getElementById("autor").value;
        type = document.getElementById("type").value;
        price = document.getElementById("price").value;
        pages = document.getElementById("pages").value;
        cover = document.getElementById("cover").value;

        //  * http://webref.pl/arena/dom/dom_pselement_innerhtml.html
        // innerHTML po lewej "setting". Ustawia wartość "tittle" w komórce "cell1"

        cell1.innerHTML = tittle;
        cell2.innerHTML = autor;
        cell3.innerHTML = type;
        cell4.innerHTML = price;
        cell5.innerHTML = pages;
        cell6.innerHTML = '<img src="' + cover + '" height="100" width="60">';
        // dodaje nowy gatunek do rozwijanego selectu
        option.innerHTML = type;
        types.appendChild(option);

        //  wywołuje funkcję, aby wywołać event do nowego wiersza poprzez pobranie nowej tablicy
        selectedRowToInput();
    }
}

function selectedRowToInput() {
    for (var i = 1; i < table.rows.length; i++) {

        table.rows[i].onclick = function () {
            rIndex = this.rowIndex;
            //innerHTML po prawej "getting" pobiera z komórku cells[0] wartość i przypisuje ją "tittle"
            document.getElementById("tittle").value = this.cells[0].innerHTML;
            document.getElementById("autor").value = this.cells[1].innerHTML;
            document.getElementById("type").value = this.cells[2].innerHTML;
            document.getElementById("price").value = this.cells[3].innerHTML;
            document.getElementById("pages").value = this.cells[4].innerHTML;
            document.getElementById("cover").value = this.cells[5].innerHTML;
        };
    }
}

function editSelectedRow() {

    var tittle = document.getElementById("tittle").value,
        autor = document.getElementById("autor").value,
        type = document.getElementById("type").value,
        price = document.getElementById("price").value,
        pages = document.getElementById("pages").value,
        cover = document.getElementById("cover").value;

    if (!checkEmptyInput()) {
        table.rows[rIndex].cells[0].innerHTML = tittle;
        table.rows[rIndex].cells[1].innerHTML = autor;
        table.rows[rIndex].cells[2].innerHTML = type;
        table.rows[rIndex].cells[3].innerHTML = price;
        table.rows[rIndex].cells[4].innerHTML = pages;
        table.rows[rIndex].cells[5].innerHTML = cover;
    }
}

function removeSelectedRow() {

    table.deleteRow(rIndex);
    document.getElementById("tittle").value = "";
    document.getElementById("autor").value = "";
    document.getElementById("type").value = "";
    document.getElementById("price").value = "";
    document.getElementById("pages").value = "";
    document.getElementById("cover").value = "";

}

function disableCheckBox() {

    var tittleCheckBox = document.getElementById("tittleSort");
    var authorCheckBox = document.getElementById("authorSort");

    tittleCheckBox.onclick = function () {
        authorCheckBox.disabled = true;
    }

    authorCheckBox.onclick = function () {
        tittleCheckBox.disabled = true;
    }
}






