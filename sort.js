var table;

function getTheSameElement(filteredList, comparedList) {
    var theSameElements = new Array();
    for (var i = 0; i < filteredList.length; i++) {
        for (var j = 0; j < comparedList.length; j++) {
            if (filteredList[i].author == comparedList[j].author
                && filteredList[i].type == comparedList[j].type
                && filteredList[i].price == comparedList[j].price
                && filteredList[i].tittle == comparedList[j].tittle
                && filteredList[i].pages == comparedList[j].pages
                && filteredList[i].cover == comparedList[j].cover) {
                theSameElements.push(comparedList[j]);
            }
        }
    }
    return theSameElements;
}

//filtrowanie każdej zwróconej listy z każdego "ifa"
function filterFinalList(filteredList, list) {
    //trzeba napisać dodatkową funkcję porównującą całe dwie listy i wstawić po "samym"else
    if (filteredList.length == 0) {
       return list;
        //zrobiona osobna fukcja getTheSameElement, ponieważ w innym wypadku trzebabybyło GetTheSameElement wstawić w warunek po else if(...)
    } else {
        return getTheSameElement(filteredList, list);
    } 
}

function sortTable() {
    table = document.getElementById("tableOfProducts");
    var sortList = new Array();
    var tittleCheckBox = document.getElementById("tittleSort");
    var authorCheckBox = document.getElementById("authorSort");
    var priceCheckBox = document.getElementById("priceSort");
    var typeCheckBox = document.getElementById("typeSort");
    // lista z pasującymi do filtrów elementami
    var finalList = new Array();
    //wypełnianie listy

    for (var i = 0; i < table.rows.length - 1; i++) {

        sortList[i] = {
            tittle: table.rows[i + 1].cells[0].innerHTML,
            author: table.rows[i + 1].cells[1].innerHTML,
            type: table.rows[i + 1].cells[2].innerHTML,
            price: table.rows[i + 1].cells[3].innerHTML,
            pages: table.rows[i + 1].cells[4].innerHTML,
            cover: table.rows[i + 1].cells[5].innerHTML,
        }
    }


    if (authorCheckBox.checked) {

        var sortedAuthorList = new Array();
        sortedAuthorList = sortList.sort(function (a, b) {

            var authorA = a.author.toUpperCase();
            var authorB = b.author.toUpperCase();

            var tabAuthorA = authorA.split(' ');
            var tabAuthorB = authorB.split(' ');

            if (tabAuthorA.length > 1) {
                var lastNameA = tabAuthorA[tabAuthorA.length - 1];
            } else {
                lastNameA = tabAuthorA[0];
            }

            if (tabAuthorB.length > 1) {
                var lastNameB = tabAuthorB[tabAuthorB.length - 1];
            } else {
                lastNameB = tabAuthorB[0];
            }

            let comparison = 0;
            if (lastNameA > lastNameB) {
                comparison = 1;
            } else if (lastNameA < lastNameB) {
                comparison = -1;
            }
            return comparison;
        });
        finalList = filterFinalList(finalList, sortedAuthorList);

    }

    if (tittleCheckBox.checked) {
        var sortedTittleList = new Array();
        sortedTittleList = sortList.sort(function (a, b) {
            const tittleA = a.tittle.toUpperCase();
            const tittleB = b.tittle.toUpperCase();

            let comparison = 0;
            if (tittleA > tittleB) {
                comparison = 1;
            } else if (tittleA < tittleB) {
                comparison = -1;
            }
            return comparison;
        });

        finalList = filterFinalList(finalList, sortedTittleList);
    }

    if (priceCheckBox.checked) {

        var startPrice = document.getElementById("startPrice").value;
        var endPrice = document.getElementById("endPrice").value;
        var matchedPrice = new Array();

        for (i = 0; i < sortList.length; i++) {
            // trzeba zmapować typ string na int, ponieważ bez tego js porównuje stringi w bajtach
            if (parseInt(startPrice) <= parseInt(sortList[i].price) && parseInt(sortList[i].price) <= parseInt(endPrice)) {
                matchedPrice.push(sortList[i]);
            }
        }
        finalList = filterFinalList(finalList, matchedPrice);
    }

    if (typeCheckBox.checked) {

        var selectedType = document.getElementById("types");
        var matchedType = new Array();
        for (var i = 0; i < sortList.length; i++) {
            if (sortList[i].type == selectedType.value) {
                matchedType.push(sortList[i]);
            }
        }
        finalList = filterFinalList(finalList, matchedType);
    }
    return finalList;
}


function insertTable(list) {
    //Stosuje j--, ponieważ z każdym skasowaniem wiersza rozmiar tabeli 
    // się zmniejsza i wystapiłby błąd list.length > table.length
    var checkBox = document.getElementsByClassName("sortCheck");
    table = document.getElementById("tableOfProducts");

    for (var i = 0; i < checkBox.length; i++) {

        if (checkBox[i].checked) {

            for (var j = table.rows.length - 1; j > 0; j--) {
                table.deleteRow(j);
            }

            if (list.length > 0) {
                // zaczynam od 1, ponieważ wiersz o indexie 0 to tablehead
                for (var k = 1; k < list.length + 1; k++) {
                    var newRow = table.insertRow(k),

                        cell1 = newRow.insertCell(0),
                        cell2 = newRow.insertCell(1),
                        cell3 = newRow.insertCell(2),
                        cell4 = newRow.insertCell(3),
                        cell5 = newRow.insertCell(4),
                        cell6 = newRow.insertCell(5);

                    cell1.innerHTML = list[k - 1].tittle;
                    cell2.innerHTML = list[k - 1].author;
                    cell3.innerHTML = list[k - 1].type;
                    cell4.innerHTML = list[k - 1].price;
                    cell5.innerHTML = list[k - 1].pages;
                    cell6.innerHTML = list[k - 1].cover;
                }
            }
        }
    }
}
